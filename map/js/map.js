﻿var mymap = (function () {

    function mymap() {
        this.CreateMarkerStyle = function () {
            var iconStyle = new ol.style.Style({
                image: new ol.style.Icon(/** @type {olx.style.IconOptions} */({
                    anchor: [0.5, 46],
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'pixels',
                    opacity: 0.75,
                    src: '../data/icon.png'
                }))
            });

            return iconStyle;
        };

        this.CreateMarkerVectorLayer = function (pointCord, title, map) {            
            var iconFeature = new ol.Feature({
                geometry: new ol.geom.Point(pointCord),
                name: title
            });

            var markerStyle = this.CreateMarkerStyle();
            iconFeature.setStyle(markerStyle);

            var vectorSource = new ol.source.Vector({
                features: [iconFeature]
            });

            var vectorLayer = new ol.layer.Vector({
                source: vectorSource
            });

            return vectorLayer;
        };
        
        this.CreatePolygonStyle = function (opacity, stokeColor, stokeWidth) {
            return new ol.style.Style({
                fill: new ol.style.Fill({
                    color: 'rgba(0, 0, 255, ' + opacity + ')'
                }),
                stroke: new ol.style.Stroke({
                    color: stokeColor,
                    width: stokeWidth
                })
            });
        }

        this.CreatePolygon = function (pointCord, name, style) {
            var feature = new ol.Feature({
                geometry: new ol.geom.Polygon(pointCord),
                name: name
            });

            if (style != null)
                feature.setStyle(style);

            var vectorSource = new ol.source.Vector({
                features: [feature]
            });

            var vectorLayer = new ol.layer.Vector({
                source: vectorSource
            });

            return vectorLayer;
        }

        this.CreatePolyLine = function (pointCord, name, style) {
            var feature = new ol.Feature({
                geometry: new ol.geom.LineString(pointCord, 'XY'),
                name: name
            });
            
            if (style != null)
                feature.setStyle(style);

            var vectorSource = new ol.source.Vector({
                features: [feature]
            });

            var vectorLayer = new ol.layer.Vector({
                source: vectorSource
            });

            return vectorLayer;
        }
    }

    return mymap;

})();
