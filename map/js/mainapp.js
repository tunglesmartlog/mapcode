﻿/// <reference path="~/js/ol3.js.js" />
/// <reference path="~/js/map.js" />

var app = angular.module('app', []);

angular.module('app').controller('defaultController', function ($rootScope, $scope) {


    var osmLayer = new ol.layer.Tile({
        source: new ol.source.OSM()
    });

    var hcm = ol.proj.transform([106.7, 10.78], 'EPSG:4326', 'EPSG:3857');

    var view = new ol.View({
        center: hcm,
        zoom: 13
    });
    
    //Create a route from quan1 -> quan2 -> quan4 -> quan5 -> quanPN -> quan1
    var quan1 = ol.proj.transform([106.7, 10.78], 'EPSG:4326', 'EPSG:3857');
    var quan2 = ol.proj.transform([106.7349, 10.7928], 'EPSG:4326', 'EPSG:3857');
    var quan3 = ol.proj.transform([106.6868, 10.7828], 'EPSG:4326', 'EPSG:3857');
    var quan4 = ol.proj.transform([106.7023, 10.7591], 'EPSG:4326', 'EPSG:3857');
    var quan5 = ol.proj.transform([106.6683, 10.7557], 'EPSG:4326', 'EPSG:3857');
    var quanPN = ol.proj.transform([106.6717, 10.7995], 'EPSG:4326', 'EPSG:3857');

    var maplib = new mymap();
    var markerlayerQ1 = maplib.CreateMarkerVectorLayer(quan1, 'Quận 1');
    var markerlayerQ2 = maplib.CreateMarkerVectorLayer(quan2, 'Quận 2');
    var markerlayerQ3 = maplib.CreateMarkerVectorLayer(quan3, 'Quận 3');
    var markerlayerQ4 = maplib.CreateMarkerVectorLayer(quan4, 'Quận 4');
    var markerlayerQ5 = maplib.CreateMarkerVectorLayer(quan5, 'Quận 5');
    var markerlayerQPN = maplib.CreateMarkerVectorLayer(quanPN, 'Quận Phú nhuận');

    var style = maplib.CreatePolygonStyle(0.1, 'red', 2);
    var polygon = maplib.CreatePolygon([[quan1, quan4, quan5]], "TEST", style);
    
    var polyline = maplib.CreatePolyLine([quanPN, quan2], "LINE", style);

    var map = new ol.Map({
        layers: [osmLayer, markerlayerQ1, markerlayerQ2, markerlayerQ3, markerlayerQ4, markerlayerQ5, markerlayerQPN, polygon],
        view: view, target: 'map'
    });

    map.addControl(polyline);

    var mousePosition = new ol.control.MousePosition({
        coordinateFormat: ol.coordinate.createStringXY(4),
        projection: 'EPSG:4326',
        target: document.getElementById('myposition'),
        undefinedHTML: '&nbsp;'
    });
    
    map.addControl(mousePosition);
    
    var popup = new ol.Overlay({
        element: document.getElementById('win')
    });
    popup.setPosition(quan1);
    map.addOverlay(popup);
    //$scope.Item = null;
    map.on('click', function (e) {
        var element = popup.getElement();
        var coordinate = e.coordinate;
        var hdms = ol.coordinate.toStringHDMS(ol.proj.transform(
            coordinate, 'EPSG:3857', 'EPSG:4326'));
       
        //$scope.Item = {
        //    Lat: 'asdasdasd', Lng: 'asdasdasd'
        //}
        popup.setPosition(coordinate);
        element.style.display = "";
    })
 });
